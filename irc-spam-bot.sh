#!/bin/bash
# Original source: https://gist.githubusercontent.com/eldstal/3979151/raw/276bd9bb5a853e4c572c7793fa2baf7c527581ae/bot.sh

rcptTo="USERNAME-WHO-SHOULD-RECEIVE-THE-SPAM"



# Nickname as $1
# The tempfile $2 gets the PING, so we can return it back
function ircauth {
  echo "NICK ${1}"
  echo "USER ${1} 0 * :Totally not a spambot"
  # Sadly the pong cannot be sent automatically, as we dont get the ping.
  # So the user has to repeat the text that is printed on the screen
  # After "PING :FFFFFFFFFFFF324" is shown, the user has to write
  # "PONG :FFFFFFFFFFFF324" followed by a line break
  read pong
  echo $pong
}

function messages {
  echo "spam spam spam"
  echo "lovely spam"
}

# stdin: a stream where each line is converted to a message
# $1: The user/channel to send to
function privmsg {
  TARGET=$1
  sed -re "s/^(.*)\$/PRIVMSG ${TARGET} :\1/"
}

function disconnect {
  echo "QUIT no more spam"
}

# Counteract irc spam detection by delaying messages.
function delay {
  while read LINE; do
    # sleep for a randomn interval between 2 and 15 seconds
    sleep $(shuf -i 2-15 -n 1)
    echo $LINE
  done
}

botUser=$(cat /dev/urandom | tr -dc 'a-zA-Z' | fold -w 32 | head -n 1)

(
 ircauth "$botUser" "$tmpfile";
 messages | privmsg "$rcptTo";
 disconnect;
) | delay | nc irc.hackint.org 6667

